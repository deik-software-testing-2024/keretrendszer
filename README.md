# Framework for UI automation

This project is for teaching how to set up and use a Java based UI automation.
The goals of the project are to add 2 initial test cases with the necessary dependencies, using Page Object Pattern, then run it and analyze the report.

# Usage

The following commands would be useful:

```mvn clean install```
It will clear the 'target' folder, then runs the tests.

```mvn clean test```
Almost the same as the previous command, the difference between them that this command will create the Allure report (allure-results folder in the root directory), which could be opened on a LiveServer with another command, saving some time from checking the results in the terminal.

```allure serve allure-result``` This command will open a LiveServer on localhost, and automatically navigates to the test results page in the default browser. If you would like to use the terminal again, it's necessary to close the LiveServer connection in the terminal with Ctrl+C.

# Notes

Will be added later, it will contain the brief description of the Page Object Pattern, what to add first, useful tips and tricks, and some more material.